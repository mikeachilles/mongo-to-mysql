# Define data types that can be converted
DATA_TYPES = { :String => "String", :Number => "Float", :Date => "Datetime", :Boolean => "Boolean", :ObjectId => "String" }

# A field is okay for processing if it hasn't been processed yet and it's parent has been processed
def ok_for_processing?(coll_sym, mysql_schema_hash, full_field_name, field_is_child)
  !mysql_schema_hash[coll_sym][:already_processed_fields].include?(full_field_name) && ( !field_is_child || (field_is_child && mysql_schema_hash[coll_sym][:already_processed_fields].include?(full_field_name.gsub(/.\w+$/,"")) ) )
end


def define_new_table(mysql_schema_hash, table_name, coll_sym=nil, parent_key=nil, parent_table_sym=nil)
  table_sym = table_name.to_sym
  mysql_schema_hash[:tables] << table_name
  mysql_schema_hash[:table_definitions][table_sym] = {}
  mysql_schema_hash[:table_definitions][table_sym][:columns] = []
  mysql_schema_hash[:table_definitions][table_sym][:indexes] = []
  mysql_schema_hash[:table_definitions][table_sym][:children] = []
  # first column is always auto_increment primary key
  mysql_schema_hash[:table_definitions][table_sym][:columns] << "primary_key :#{table_name}_id"
  mysql_schema_hash[:table_definitions][table_sym][:parent] = parent_table_sym
  unless parent_key.nil?
    #puts "table_sym = #{table_sym} and parent_table_sym is #{parent_table_sym}"
    mysql_schema_hash[:table_definitions][parent_table_sym][:children] << table_sym
    mysql_schema_hash[coll_sym][:child_tables][parent_key] = table_sym
    parent_reference_column_name = dot_to_underscore(parent_table_sym.to_s)
    mysql_schema_hash[:table_definitions][table_sym][:columns] << "integer :#{parent_reference_column_name}_id, :null => false"
    mysql_schema_hash[:table_definitions][table_sym][:indexes] << "index :#{parent_reference_column_name}_id, :name => 'parent_column_index'" 
  end
  return mysql_schema_hash
end


def ensure_parent_rows_are_created(rows_to_insert, field_key, column_map_hash) # column_map_hash[field_key][:parent_field_key])
  # Think this needs mysql_schema_hash[:table_definitions][table_sym][:parent]
  puts "this is the function for finding or creating parent table entries.  Looking for one for #{field_key}"
  #puts "column_map_hash keys : #{column_map_hash.keys.to_s}"

  begin
    table_sym = column_map_hash[field_key][:table_symbol]
    if rows_to_insert[table_sym]
      puts "parent row: #{field_key} with table_sym: #{table_sym} already exists"
    else
      puts "creating parent row: #{field_key} with table_sym: #{table_sym}"
      # TODO check if this could cause problems if there are already existing parent rows
      rows_to_insert[table_sym] = {} unless rows_to_insert[table_sym]
      rows_to_insert[table_sym][0] = {} unless rows_to_insert[table_sym][0]
      puts "creating: #{table_sym.to_s} #{rows_to_insert[table_sym]}"
      parent_field_key = column_map_hash[field_key][:parent_field_key]
      puts "if this is not empty then there's a parent: #{parent_field_key}"
      # recurse if parent is present
      unless parent_field_key.nil?
        rows_to_insert = ensure_parent_rows_are_created(rows_to_insert, parent_field_key, column_map_hash )
      end
    end
  rescue
    abort "Cannot create parent row.  Variables used are: parent_field_key: #{field_key}, table_sym: #{column_map_hash[field_key][:table_symbol]}"
  end
  return rows_to_insert
end


def ensure_parent_fields_are_mapped(coll_sym, collection_mysql_schema_hash, field_name)
  field_key = make_key(field_name)
  table_sym = (coll_sym.to_s + "_" + dot_to_underscore(field_name)).to_sym
  puts "Mapping #{field_name} to #{table_sym.to_s}"

  collection_mysql_schema_hash[:column_map][field_key] = {} unless collection_mysql_schema_hash[:column_map][field_key]
  collection_mysql_schema_hash[:column_map][field_key][:table_symbol] = table_sym
  
  if check_if_child(field_name)
    collection_mysql_schema_hash[:column_map][field_key][:parent_field_key] = make_key(field_name.gsub(/.\w+$/,""))
    parent_field_name = field_name.gsub(/.\w+$/,"")
    collection_mysql_schema_hash =  ensure_parent_fields_are_mapped(coll_sym, collection_mysql_schema_hash, parent_field_name)
  end
  return collection_mysql_schema_hash
end

######################################################################################################################
# Main parsing method
# coll_sym is the collection being processed in symbol form
# all_fields is the varietyResuts collection
# field_info is varietyResults row for the field being processed
# collection_mysql_schema_hash is where we store all mongo to mysql mapping info for the collection being processed
# current_table is the one we are looking to put this field to (unless it has multiple types or is an array or object)
# parent_field is the name of the parent object
# parent keys is the field_key of the parent object
def convert_field(coll_sym, all_fields, field_info, mysql_schema_hash, current_table_sym, parent_field, parent_keys)
  current_table = current_table_sym.to_s
  full_field_name = field_info["_id"]["key"].include?(".XX.") ? field_info["_id"]["key"].gsub(".XX.",".") : field_info["_id"]["key"]
  field_is_child = check_if_child(full_field_name)  #full_field_name.include?(".")

  # You need to track already processed fields because the calling loop might process again what was already handled by recursion
  # Also do not handle nested objects unless direct child of parent_field
  if ok_for_processing?(coll_sym, mysql_schema_hash, full_field_name, field_is_child)
    
    # TODO revisit this we might have redundant variables
    if field_is_child
      parent_field_name = full_field_name.gsub(/.\w+$/,"")
      parent_table_sym = (coll_sym.to_s + "_" + dot_to_underscore(parent_field_name)).to_sym
      parent_key = make_key(parent_field_name)
      grandpa_key = make_key(parent_field_name.gsub(/.\w+$/,""))
      field_name = full_field_name.gsub(/^#{parent_field_name}./,"")
    else
      parent_field_name = current_table
      parent_table_sym = current_table_sym
      field_name = full_field_name
      parent_key = nil
    end
    
    field_key = make_key(full_field_name)

    puts "parent_field_name: #{parent_field_name} --- parent_key: #{parent_key} --- full_field_name: #{full_field_name} --- field_key: #{field_key} --- processed_keys: #{mysql_schema_hash[coll_sym][:already_processed_fields].to_s}"
    
    # Include in array of processed fields
    mysql_schema_hash[coll_sym][:already_processed_fields] << full_field_name

    collection_mysql_schema_hash = mysql_schema_hash[coll_sym]
  
    # Different mongo key types will have to be handled differently
    if field_info["value"].keys.first == "types"
      # handle multiple types
      field_types = field_info["value"]["types"]
      puts "****** Handling multiple types: #{field_types.to_s}"
      
      if field_types.include?("Array") || field_types.include?("Object")
        # Exactly same handling as if lone field_type of Array?
        puts "One of them is an array: #{full_field_name}"
        # Arrays may contain simple data or objects or other arrays
        # Definitely this will cause at least one child table --> same way as you created an object with addition of one column named after full_field_name
        new_table = current_table + "_" + dot_to_underscore(full_field_name)
        mysql_schema_hash = define_new_table(mysql_schema_hash, new_table, coll_sym, field_key, parent_table_sym)
        mysql_schema_hash[coll_sym][:column_map][field_key] = {}
        #mysql_schema_hash[coll_sym][:already_processed_fields] << full_field_name + ".XX"

#        # If at least one of the field types is not an array or object
#        matching_fields = []
#        all_fields.each do |a_field_name|
#          if a_field_name.include?(full_field_name)
#            matching_fields << a_field_name
#          end
#        end
#        puts "child fields: #{matching_fields}"
#        if matching_fields.size > 1
#          puts "Setting column_type to ArrayWithObjects"
#
#          collection_mysql_schema_hash[:column_map][field_key][:column_type] = :ArrayWithObjects
#        else
        table_for_field = new_table   #the created new table
      else
        table_for_field = current_table #the parent table
      end
         ### ERROR table_for_field is not defined.  If
          puts "Setting column type to Array and creating String column #{new_table}.#{full_field_name}"
          collection_mysql_schema_hash[:column_map][field_key][:column_type] = :ArrayOrString
          collection_mysql_schema_hash[:column_map][field_key][:table_symbol] = new_table.to_sym
          collection_mysql_schema_hash[:column_map][field_key][:column_symbol] = full_field_name.to_sym
          collection_mysql_schema_hash[:column_map][field_key][:parent_field_key] = field_is_child ? grandpa_key : nil
          if field_is_child
            puts "### Mapping parent fields"
            collection_mysql_schema_hash = ensure_parent_fields_are_mapped(coll_sym, collection_mysql_schema_hash, parent_field_name)
          end

          puts "#### created column_map: #{collection_mysql_schema_hash[:column_map][field_key]}"

          mysql_schema_hash[:table_definitions][new_table.to_sym][:columns] << "String :#{field_name.to_s}"
          puts "table for array defined as: #{mysql_schema_hash[:table_definitions][new_table.to_sym][:columns]}"
#        end
      
        # Always create a column on the table for the string key of the same name
#        field_type = :ArrayOrString
#        collection_mysql_schema_hash[:column_map][field_key] = {}
#        table_for_field = field_is_child ? mysql_schema_hash[coll_sym][:child_tables][parent_key] : current_table_sym
#        collection_mysql_schema_hash[:column_map][field_key][:table_symbol] = table_for_field
#        collection_mysql_schema_hash[:column_map][field_key][:column_symbol] = field_name.to_sym
#        collection_mysql_schema_hash[:column_map][field_key][:column_type] = field_type
#        puts "field_key: #{field_key} --- parent_key: #{parent_key}"
#        collection_mysql_schema_hash[:column_map][field_key][:parent_field_key] = field_is_child ? grandpa_key : nil
#        if field_is_child
#          puts "### Mapping parent fields"
#          collection_mysql_schema_hash = ensure_parent_fields_are_mapped(coll_sym, collection_mysql_schema_hash, parent_field_name)
#        end
#        puts "#### created column_map: #{collection_mysql_schema_hash[:column_map][field_key]}"

#        puts "full_field_name: #{full_field_name} --- current_table: #{current_table} --- table_for_field: #{table_for_field} --- known_child_tables: #{mysql_schema_hash[coll_sym][:child_tables]} -- table_definition_keys: #{mysql_schema_hash[:table_definitions].keys.to_s}"
        # Create column definition
#        mysql_schema_hash[:table_definitions][table_for_field][:columns] << "#{DATA_TYPES[field_type]} :#{field_name.to_s}"


#      end

    else
      field_type = field_info["value"].values.first.to_sym
      override_setting_present = setting_present?(IMPORT_SETTINGS, :OVERRIDE_TYPE_FOR) && setting_present?(IMPORT_SETTINGS[:OVERRIDE_TYPE_FOR], coll_sym.to_s) && setting_present?(IMPORT_SETTINGS[:OVERRIDE_TYPE_FOR][coll_sym.to_s], full_field_name)

      if field_type == :Array && !override_setting_present
        puts "#### Handling an Array: #{full_field_name}"
        # Arrays may contain simple data or objects or other arrays
        # Definitely this will cause at least one child table --> same way as you created an object with addition of one column named after full_field_name
        new_table = current_table + "_" + dot_to_underscore(full_field_name)
        mysql_schema_hash = define_new_table(mysql_schema_hash, new_table, coll_sym, field_key, parent_table_sym)
        mysql_schema_hash[coll_sym][:column_map][field_key] = {}
        #mysql_schema_hash[coll_sym][:already_processed_fields] << full_field_name + ".XX"

        # If there are no child fields then should create a string column based on the full field name
        matching_fields = []
        all_fields.each do |a_field_name|
          if a_field_name.include?(full_field_name)
            matching_fields << a_field_name
          end
        end
        puts "child fields: #{matching_fields}"
        if matching_fields.size > 1
          puts "Setting column_type to ArrayWithObjects"
          collection_mysql_schema_hash[:column_map][field_key][:column_type] = :ArrayWithObjects
        else
          puts "Setting column type to Array and creating String column #{new_table}.#{field_name}"
          collection_mysql_schema_hash[:column_map][field_key][:column_type] = :Array
          collection_mysql_schema_hash[:column_map][field_key][:table_symbol] = new_table.to_sym
          collection_mysql_schema_hash[:column_map][field_key][:column_symbol] = field_name.to_sym
          collection_mysql_schema_hash[:column_map][field_key][:parent_field_key] = field_is_child ? parent_key : nil
          if field_is_child
            puts "### Mapping parent fields"
            collection_mysql_schema_hash = ensure_parent_fields_are_mapped(coll_sym, collection_mysql_schema_hash, parent_field_name)
          end
      
          puts "#### created column_map: #{collection_mysql_schema_hash[:column_map][field_key]}"
          
          mysql_schema_hash[:table_definitions][new_table.to_sym][:columns] << "String :#{field_name.to_s}"
          puts "table for array defined as: #{mysql_schema_hash[:table_definitions][new_table.to_sym][:columns]}"
        end
        
      elsif field_type == :Object
        # An object will always result in another table so we need to define a new table and record it in hash
        new_table = current_table + "_" + dot_to_underscore(full_field_name)
        mysql_schema_hash = define_new_table(mysql_schema_hash, new_table, coll_sym, field_key, parent_table_sym)
        
      else
        field_type = override_setting_present ? IMPORT_SETTINGS[:OVERRIDE_TYPE_FOR][coll_sym.to_s][full_field_name].to_sym : field_type

        collection_mysql_schema_hash[:column_map][field_key] = {}
        table_for_field = field_is_child ? mysql_schema_hash[coll_sym][:child_tables][parent_key] : current_table_sym
        collection_mysql_schema_hash[:column_map][field_key][:table_symbol] = table_for_field
        collection_mysql_schema_hash[:column_map][field_key][:column_symbol] = field_name.to_sym
        collection_mysql_schema_hash[:column_map][field_key][:column_type] = field_type
        puts "field_key: #{field_key} --- parent_key: #{parent_key}"
        collection_mysql_schema_hash[:column_map][field_key][:parent_field_key] = field_is_child ? grandpa_key : nil
        if field_is_child
          puts "### Mapping parent fields"
          collection_mysql_schema_hash = ensure_parent_fields_are_mapped(coll_sym, collection_mysql_schema_hash, parent_field_name)
        end
        puts "#### created column_map: #{collection_mysql_schema_hash[:column_map][field_key]}"
       
        puts "full_field_name: #{full_field_name} --- current_table: #{current_table} --- table_for_field: #{table_for_field} --- known_child_tables: #{mysql_schema_hash[coll_sym][:child_tables]} -- table_definition_keys: #{mysql_schema_hash[:table_definitions].keys.to_s}"
        # Create column definition
        mysql_schema_hash[:table_definitions][table_for_field][:columns] << "#{DATA_TYPES[field_type]} :#{field_name.to_s}"

        # This should really belong either at the start or end of the processing block, but here first for debugging
        #mysql_schema_hash[coll_sym][:already_processed_fields] << full_field_name
      end 
    end
  end
  return mysql_schema_hash
end

