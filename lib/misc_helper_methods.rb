## MISCELLANEOUS HELPER METHODS

# For executing code that should cause the script to abort if failed. so that you don't need to do a begin-rescue-end each time
def rescue_this_with(error_message, &your_proc)
  begin
    your_proc.call
  rescue
    abort error_message
  end
end

def symbolize(name, required, loaded_yaml)
  symbolized_settings = loaded_yaml.symbolize_keys!
  return symbolized_settings
rescue
  if required
    abort "Unable to load #{name} settings. Exiting."
  else
    return nil
  end
end

def setting_present?(settings_hash, setting_key)
  !settings_hash.nil? && settings_hash.has_key?(setting_key) && !settings_hash[setting_key].nil?
end

def set_to(settings_hash, setting_key)
  setting_present?(settings_hash, setting_key) && settings_hash[setting_key]
end

# Create field_name and field_key from the collection key name
# if mongodb field is something like a : {b : "value"} field_key will be ['a']['b']
def make_key(field_name)
  "['" + field_name.gsub(".","']['") + "']"
end

#
def dot_to_underscore(this_string)
  this_string.gsub(".","_")
end

def check_if_child(field_name)
  field_name.include?(".")
end

