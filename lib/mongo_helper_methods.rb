# MongoDB helper methods
def mongo_connect(db_type, mongo_settings, database_name)
  rescue_this_with("Unable to connect to #{db_type} Mongo database.  Please check connection settings.") do
    @connection_info = MongoClient.new(mongo_settings[:host], mongo_settings[:port]).db(database_name)
    unless mongo_settings[:user].nil?
      unless @connection_info.authenticate(mongo_settings[:user], mongo_settings[:password])
        abort "Authentication to source Mongo database failed.  Please check username and password. Exiting"
      end
    end
  end
  return @connection_info
end

def mongo_auth_string(mongo_settings)
  !mongo_settings[:user].nil? ? "-u #{mongo_settings[:user]} -p #{mongo_settings[:password]}" : ""
end

def system_mongo(mongo_settings, mongo_database_name, eval_string, js_file)
  auth_string = mongo_auth_string(mongo_settings)
  system "mongo #{auth_string} #{mongo_settings[:host]}:#{mongo_settings[:port]}/#{mongo_database_name} --eval \"#{eval_string}\" #{js_file}"
end

#TODO it is recommended that this uses mongodump and mongorestore instead
def system_mongo_import_export(mongo_settings, mongo_database_name, request_type, collection_name)
  auth_string = mongo_auth_string(mongo_settings)
  if ["export","import"].include?(request_type)
    puts "#{request_type}ing #{collection_name}"
    file_param = request_type=="export" ? "--out" : "--file"
    system "mongo#{request_type} #{auth_string} --host #{mongo_settings[:host]} --port #{mongo_settings[:port]} --db #{mongo_database_name} --collection #{collection_name} #{file_param} mongoexport.json"
  else
    abort "Request should only be 'import' or 'export', not #{request_type}. Somebody messed something up. Exiting."
  end
end


