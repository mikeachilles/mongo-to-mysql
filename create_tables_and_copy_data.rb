require_relative "lib/mongo_helper_methods"
require_relative "lib/misc_helper_methods"
require_relative "lib/parsing_methods"
require "mysql2"
require "mongo"
require "sequel"
require "json"
require "yaml"

include Mongo
settings_file = ARGV[0].nil? ? "settings.yml" : ARGV[0]
CLONE_COLLECTION_JS_FILE = "clone_collection.js"
VARIETY_JS_FILE = "variety.js"
VARIETY_MONGO_DB_NAME = "varietyResults"

#####################################################################################################################
# MAIN PROCEDURE STARTS HERE
rescue_this_with("Unable to load #{settings_file}. Exiting") do
  @all_settings = YAML::load(File.open(settings_file))
end

MYSQL_SETTINGS = symbolize("MySQL", true, @all_settings['CONNECTION_SETTINGS']['MYSQL'])
SOURCE_MONGO_SETTINGS = symbolize("Source MongoDB", true, @all_settings['CONNECTION_SETTINGS']['MONGO']['SOURCE'])
SOURCE_MONGO_SETTINGS[:port] = 27017 unless SOURCE_MONGO_SETTINGS[:port]
MONGO_DATABASE_NAME = @all_settings['CONNECTION_SETTINGS']['MONGO']['DATABASE_NAME']
LOCAL_MONGO_SETTINGS = symbolize("Local MongoDB", false, @all_settings['CONNECTION_SETTINGS']['MONGO']['LOCAL'])
IMPORT_SETTINGS = symbolize("Import Settings", false, @all_settings['IMPORT_SETTINGS'])

destination_mysql = Sequel.connect(MYSQL_SETTINGS)

source_mongo = mongo_connect("SOURCE", SOURCE_MONGO_SETTINGS, MONGO_DATABASE_NAME)

# DETERMINE COLLECTIONS TO IMPORT
collections = setting_present?(IMPORT_SETTINGS, :collections) ? IMPORT_SETTINGS[:collections] : source_mongo.collection_names

  
using_local_mongo = !LOCAL_MONGO_SETTINGS.nil? && !LOCAL_MONGO_SETTINGS[:host].nil?

if using_local_mongo
  final_source_mongo = mongo_connect("LOCAL", LOCAL_MONGO_SETTINGS, MONGO_DATABASE_NAME)
  final_source_mongo_settings = LOCAL_MONGO_SETTINGS
else
  final_source_mongo = source_mongo
  final_source_mongo_settings = SOURCE_MONGO_SETTINGS
end

variety_mongo = mongo_connect("Variety", final_source_mongo_settings, VARIETY_MONGO_DB_NAME)
local_mongo_collections = final_source_mongo.collection_names unless using_local_mongo == false
backed_up_tables = []

mysql_schema_hash = {}
mysql_schema_hash[:tables] = []
mysql_schema_hash[:table_definitions] = {}

# PARSE COLLECTIONS
collections.each do |coll|
  # Ignore collections that start with "system."
  
#  puts "\n### Invoking variety.js to get fields for #{coll}"
#  eval_string = "var collection = '#{coll}'"
#  system_mongo(final_source_mongo_settings, MONGO_DATABASE_NAME, eval_string, VARIETY_JS_FILE)
  variety_results = variety_mongo.collection("#{coll}Keys")
  

  coll_sym = coll.to_sym
  mysql_schema_hash[coll_sym] = {}
  mysql_schema_hash[coll_sym][:column_map] = {}
  mysql_schema_hash[coll_sym][:already_processed_fields] = []
  mysql_schema_hash[coll_sym][:child_tables] = {}

  mysql_schema_hash = define_new_table(mysql_schema_hash, coll)
  
  all_fields = []
  variety_results.find.each do |row|
    all_fields << row["_id"]["key"]
  end

  # READ FROM VARIETY RESULTS AND DETERMINE MYSQL TABLE STRUCTURE, INCLUDE PARENT_TABLE_ID FOR CHILD TABLES
  while mysql_schema_hash[coll_sym][:already_processed_fields].size < variety_results.size
    variety_results.find.each do |row|
      # coll_sym is also current_table at the start, that's why there are two coll_syms in the params below
      mysql_schema_hash = convert_field(coll_sym, all_fields, row, mysql_schema_hash, coll_sym, "", "")
    end
  end
  
  puts "Processed: #{mysql_schema_hash[coll_sym][:already_processed_fields]}"
  puts "keys are: #{mysql_schema_hash[coll_sym][:column_map].keys.to_s}"

  puts "\n###Table definitions for collection #{coll} finished.  Now creating tables"

  mysql_schema_hash[:tables].each do |table|
    table_create_code = "destination_mysql.create_table(:#{table}) do\n"

    # column definitions
    mysql_schema_hash[:table_definitions][table.to_sym][:columns].each do |col|
      table_create_code = table_create_code + "#{col}\n"
    end

    # index definitions
    mysql_schema_hash[:table_definitions][table.to_sym][:indexes].each do |index|
      table_create_code = table_create_code + "#{index}\n"
    end    
    table_create_code = table_create_code + "end"

    # RENAME EXISTING TABLES, IF ANY.  PUT PENDING DELETE TABLE NAMES TO HASH
    if destination_mysql.table_exists?(table.to_sym)
      backup_table = table + "_pending_delete"
      backed_up_tables << backup_table

     if set_to(IMPORT_SETTINGS, :drop_preexisting_pending_delete_tables) && destination_mysql.table_exists?(backup_table.to_sym)
       destination_mysql.drop_table(backup_table.to_sym)
     end

      rescue_this_with("Unable to rename #{table} to #{backup_table}. A table named #{backup_table} most likely still exists. Exiting.") do
        destination_mysql.rename_table table.to_sym, backup_table.to_sym
      end
    end

    # Now create the table
    rescue_this_with("Unable to create MySQL table #{table}. Table creation code is:\n#{table_create_code}\n. You'll need that if you plan to report an issue") do
      eval(table_create_code)
    end

  end
end

puts "Done creating MySQL tables.  If an error occurs after this and you configured a local instance you may want to reconfigure the script and make the LOCAL MongoDB settings the SOURCE (and keep the LOCAL settings empty).  This way it won't redo copying the collections from source to local."

# Now do the copying
collections.each do |coll|
  puts "\n### Copying data for #{coll}"
  coll_sym = coll.to_sym
  column_map_hash = mysql_schema_hash[coll_sym][:column_map]
  field_keys = mysql_schema_hash[coll_sym][:column_map].keys

  actual_collection = final_source_mongo.collection(coll)
  #puts actual_collection
  
  actual_collection.find({}, :timeout => false) do |cursor|
    cursor.each do |row|
    rows_to_insert = {}
    puts "row => #{row}"
    # define rows to insert
    field_keys.each do |field_key|
      begin
        value = eval("row#{field_key}")
        column_type = column_map_hash[field_key][:column_type]
        unless value.nil? || value.class == BSON::OrderedHash 
          puts "#{field_key} is a #{column_type} value class is #{value.class}"

          if column_type == :ArrayOrString
            values = []
	    if value.class == Array
              values = value
            else
              values << value
            end

            table_sym = column_map_hash[field_key][:table_symbol]
            column_sym = column_map_hash[field_key][:column_symbol]
            rows_to_insert[table_sym] = {} unless rows_to_insert[table_sym]

            counter = 0
            puts "array values are #{values}"
            values.each do |v|
              puts "creating child row for #{v.to_s}"
              rows_to_insert[table_sym][counter] = {} unless rows_to_insert[table_sym][counter]
              rows_to_insert[table_sym][counter][column_sym] = v.to_s
              counter += 1
              puts rows_to_insert[table_sym]
            end
            puts "rows to insert from array: #{rows_to_insert[table_sym]}"              

          elsif column_type == :ArrayWithObjects
            #Use child keys 
            puts "array of objects for insertion: #{value}"
            puts "finding child keys of #{field_key}"
            field_keys.each do |field_key2|
              if !(field_key2.match(/^#{Regexp.escape(field_key)}/).nil?) && field_key2 != field_key 
                puts "#{field_key2} is a candidate"
                field_subkey = field_key2.gsub(field_key,"") #.gsub("['","[:").gsub("']","]")
                puts "evaluating: #{field_subkey} on each member"

                value.each do |v|

                  puts "array member: #{v}"
                  begin
                    value2 = eval("v#{field_subkey}")
                    puts "Found value: #{value2}"
                  rescue
                    value2 = nil
                  end

                  unless value2.nil? || value2.class == BSON::OrderedHash
                    #TODO what if it contains another damn array?  This should really be recursive
                     
                    value2 = column_map_hash[field_key2][:column_type] == :ObjectId ? value2.to_s : value2
                    puts "#{field_key2} => #{value2} value's class is #{value2.class}"
                    table_sym = column_map_hash[field_key2][:table_symbol]
                    column_sym = column_map_hash[field_key2][:column_symbol]
                    rows_to_insert[table_sym] = {} unless rows_to_insert[table_sym]
                    rows_to_insert[table_sym][0] = {} unless rows_to_insert[table_sym][0]
                    rows_to_insert[table_sym][0][column_sym] = value2
                    puts "assigned #{table_sym.to_s}.#{column_sym.to_s} = #{value2}"
                    puts "parent field key: #{column_map_hash[field_key2][:parent_field_key]} if this is not null or equal to field key you should be calling the function"

                    unless column_map_hash[field_key2][:parent_field_key].nil? #|| column_map_hash[field_key2][:parent_field_key] == field_key
                      puts "calling the function!"
                      rows_to_insert = ensure_parent_rows_are_created(rows_to_insert, column_map_hash[field_key2][:parent_field_key], column_map_hash)
                    end
                    #puts "This is at the end of the main insertion loop"
                  end
                  #puts "This is the end for array member #{v}"
                end
                #puts "This is the end for child key #{field_key2}"
              end
            end

          elsif column_map_hash[field_key][:column_type] == :Array
            table_sym = column_map_hash[field_key][:table_symbol]
            column_sym = column_map_hash[field_key][:column_symbol]
            rows_to_insert[table_sym] = {} unless rows_to_insert[table_sym]

            counter = 0
            puts "array values are #{value}"
            value.each do |v|
              puts "creating child row for #{v.to_s}"
              rows_to_insert[table_sym][counter] = {} unless rows_to_insert[table_sym][counter]
              rows_to_insert[table_sym][counter][column_sym] = v.to_s
              counter += 1
              puts rows_to_insert[table_sym]
            end
            puts "rows to insert from array: #{rows_to_insert[table_sym]}"

          else
            # Convert ObjectId's to string, keep other data types as is
            value = column_map_hash[field_key][:column_type] == :ObjectId ? value.to_s : value
            puts "#{field_key} => #{value} value's class is #{value.class}"
            table_sym = column_map_hash[field_key][:table_symbol]
            column_sym = column_map_hash[field_key][:column_symbol]
            rows_to_insert[table_sym] = {} unless rows_to_insert[table_sym]
            rows_to_insert[table_sym][0] = {} unless rows_to_insert[table_sym][0]
            rows_to_insert[table_sym][0][column_sym] = value
            puts "assigned #{table_sym.to_s}.#{column_sym.to_s} = #{value}"
            puts "parent field key: #{column_map_hash[field_key][:parent_field_key]} if this is not null you should be calling the function"
            unless column_map_hash[field_key][:parent_field_key].nil?
              puts "calling the function!"
              rows_to_insert = ensure_parent_rows_are_created(rows_to_insert, column_map_hash[field_key][:parent_field_key], column_map_hash)
            end
          end
          puts "rows_to_insert keys: #{rows_to_insert.keys.to_s}"
          
          end
      rescue
        # do nothing this is needed for because even if a[b] is not present this loop will still check for a[b][c]
      end
    end

    puts "rows to insert:"
    rows_to_insert.each do |k,v|
      puts "#{k} => #{v}"
    end

     #{rows_to_insert.keys}"
    # Actually insert the rows
    mysql_schema_hash[:tables].each do |table_name|
      table_sym = table_name.to_sym
      unless rows_to_insert[table_sym].nil?
      rows_to_insert[table_sym].keys.each do |row_key|

        puts "Inserting #{rows_to_insert[table_sym][row_key].to_s} to #{table_name}"

        new_id = destination_mysql[table_name.to_sym].insert(rows_to_insert[table_sym][row_key])
      
        #set reference column values for child tables
        mysql_schema_hash[:table_definitions][table_sym][:children].each do |child_table_sym|
          #puts "child table: #{child_table_sym} --- rows_to_insert.keys: #{rows_to_insert.keys.to_s}"
          if rows_to_insert.keys.include?(child_table_sym)
            puts "creating child column entry(ies) for: #{table_name}_id"
            rows_to_insert[child_table_sym].keys.each do |child_row_key|
              rows_to_insert[child_table_sym][child_row_key] = {} unless rows_to_insert[child_table_sym][child_row_key]
              rows_to_insert[child_table_sym][child_row_key][(child_table_sym.to_s + "_id").to_sym] = nil
              rows_to_insert[child_table_sym][child_row_key][(table_name + "_id").to_sym] = new_id
            end
          end
        end
      end
      end
    end
    end
  end

  
  # Use mysql_schema_hash[:collection][:column_map][field_key] 
  # GET A ROW, USE FIELD KEYS GET AND STORE VALUES TO APPROPRIATE TABLE
  # INSERT ROWS IN SEQUENCE.  CHILD TABLES SHOULD GET ID FROM PARENT

end


# DROP OLD TABLES FROM PENDING DELETE TABLE HASH
# DB.drop_table(:table_name)
# Drop varietyResults - should include in settings with default=true
puts "\n### Cleaning up"
system_mongo(final_source_mongo_settings, VARIETY_MONGO_DB_NAME, "db.dropDatabase()", "") unless set_to(IMPORT_SETTINGS, :do_not_delete_variety_results)
unless set_to(IMPORT_SETTINGS, :do_not_drop_created_pending_delete_tables)
  backed_up_tables.each do |table_for_deletion|
    destination_mysql.drop_table(table_for_deletion.to_sym)
  end
end


